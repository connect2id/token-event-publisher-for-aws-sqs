# Connect2id Server Token Event Publisher for AWS Simple Queue Service (SQS)

Copyright (c) Connect2idLtd., 2018 - 2022

Publishes ID and access token issue events to an AWS Simple Queue Service
(SQS). Implements the IDTokenIssueEventListener and
AccessTokenIssueEventListener SPIs from the Connect2id server SDK (version
3.26.1+).


## Published event format

The published events are JSON objects which include selected claims from the
issued token.

Example published event with the subject and the issue time of the token, the
remaining token claims are excluded:

```
{
  "sub" : "alice",
  "iat" : 1523304728
}
```


## Configuration

The publisher is configured by Java properties loaded from
`WEB-INF/tokenEventSQSPublisher.properties` or from system properties.

* op.events.sqsPublisher.enable -- If `true` enables the event publisher, if
  `false` or not set disables it. If disabled all other configuration
  properties will be ignored.

* op.events.sqsPublisher.queueURL -- The URL of the AWS SQS to publish the
  messages to. The SQS client must have the appropriate credentials and
  permissions to write to the queue.

  Example: `https://sqs.eu-central-1.amazonaws.com/673938396/MyQueue`

* op.events.sqsPublisher.includeClaims -- The names of the token claims to
  include in the published messages as space and / or comma separated list. If
  set to asterisk (`*`) all claims will be included.

  Example: `iss sub iat`


* op.events.sqsPublisher.includeSystemProperties -- The names of system
  properties to include in the published messages, blank if none.

  Example: `java.version`


## Change log

* version 1.0 (2018-04-10)
    * First release.

* version 1.1 (2018-07-12)
    * If no configuration properties or file is provided the SQS event
      publisher will stay disabled.
    * Defaults the op.events.sqsPublisher.enable configuration property to
      false (disabled).
    * Removes white space from log line IDs (Connect2id server convention).

* version 1.1.1 (2021-02-16)
    * Updates dependencies.

* version 1.1.2 (2022-01-05)
    * Updates dependencies.

* version 1.1.3 (2022-04-29)
    * Updates dependencies. 


Questions or comments? Email support@connect2id.com

