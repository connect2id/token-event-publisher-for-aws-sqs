package com.nimbusds.openid.connect.provider.spi.events.aws.sqs;


import java.io.InputStream;
import java.util.Properties;

import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.nimbusds.openid.connect.provider.spi.InitContext;
import com.nimbusds.openid.connect.provider.spi.events.*;
import net.jcip.annotations.ThreadSafe;
import net.minidev.json.JSONObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * Publishes ID and access token events to an AWS Simple Queue Service (SQS).
 */
@ThreadSafe
public class TokenEventSQSPublisher implements IDTokenIssueEventListener, AccessTokenIssueEventListener {
	
	
	/**
	 * The configuration file path.
	 */
	public static final String CONFIG_FILE_PATH = "/WEB-INF/tokenEventSQSPublisher.properties";
	
	
	/**
	 * The configuration.
	 */
	private Configuration config;
	
	
	/**
	 * The claims filter.
	 */
	private ClaimsFilter claimsFilter;
	
	
	/**
	 * The AWS SQS client.
	 */
	private AmazonSQS sqs;
	
	
	/**
	 * Log4j2 logger.
	 */
	private Logger log = LogManager.getLogger("MAIN");
	
	
	@Override
	public void init(final InitContext initContext)
		throws Exception {
		
		// Load the configuration
		Properties props = new Properties();
		
		InputStream inputStream = initContext.getResourceAsStream(CONFIG_FILE_PATH);

		if (inputStream != null) {
			props.load(inputStream);
		} else  {
                        log.info("[EVSQS0010] Couldn't find token event SQS publisher configuration file: " + CONFIG_FILE_PATH);
                }

                config = new Configuration(props); // supports sys prop override
                config.log();
                claimsFilter = new ClaimsFilter(config);
                
                // Create SQS client
		sqs = AmazonSQSClientBuilder.defaultClient();
	}
	
	
	/**
	 * Returns the configuration.
	 *
	 * @return The configuration.
	 */
	public Configuration getConfiguration() {
		
		return config;
	}
	
	
	@Override
	public boolean isEnabled() {
		return config.enable;
	}
	
	
	@Override
	public void accessTokenIssued(final AccessTokenIssueEvent event, final EventContext eventContext) {
		JSONObject filteredClaims = claimsFilter.filter(event.getJWTClaimsSet());
		sendMessage(filteredClaims);
	}
	
	
	@Override
	public void idTokenIssued(final IDTokenIssueEvent event, final EventContext eventContext) {
		JSONObject filteredClaims = claimsFilter.filter(event.getJWTClaimsSet());
		sendMessage(filteredClaims);
	}
	
	
	/**
	 * Sends the specified JSON object into the configured queue.
	 *
	 * @param jsonObject The JSON object. If {@code null} nothing will be
	 *                   sent.
	 */
	void sendMessage(final JSONObject jsonObject) {
		
		if (! config.enable || jsonObject == null) return;
		
		SendMessageRequest send_msg_request = new SendMessageRequest()
			.withQueueUrl(config.queueURL.toString())
			.withMessageBody(jsonObject.toJSONString())
			.withDelaySeconds(5);
		
		sqs.sendMessage(send_msg_request);
	}
	
	
	@Override
	public void shutdown() {
		sqs.shutdown();
	}
}
