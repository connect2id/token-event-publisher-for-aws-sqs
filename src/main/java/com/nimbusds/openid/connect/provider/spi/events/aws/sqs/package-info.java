/**
 * Token Event Publisher for AWS Simple Queue Service (SQS).
 */
package com.nimbusds.openid.connect.provider.spi.events.aws.sqs;