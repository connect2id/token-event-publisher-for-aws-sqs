package com.nimbusds.openid.connect.provider.spi.events.aws.sqs;


import java.net.URI;
import java.util.Collections;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import com.nimbusds.common.config.LoggableConfiguration;
import com.nimbusds.common.config.StringListParser;
import com.thetransactioncompany.util.PropertyParseException;
import com.thetransactioncompany.util.PropertyRetriever;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * The token event publisher configuration. It is typically derived from a Java
 * key / value properties file. The configuration is stored as public fields
 * which become immutable (final) after their initialisation.
 *
 * <p>Example configuration properties:
 *
 * <pre>
 * op.events.sqsPublisher.enable = true
 * op.events.sqsPublisher.queueURL = https://sqs.eu-central-1.amazonaws.com/673938396/MyQueue
 * op.events.sqsPublisher.includeClaims = iss sub iat
 * op.events.sqsPublisher.includeSystemProperties = tid
 * </pre>
 */
public final class Configuration implements LoggableConfiguration {
	
	
	/**
	 * The properties prefix.
	 */
	public static final String PREFIX = "op.events.sqsPublisher.";
	
	
	/**
	 * Flags whether the token event publisher is enabled.
	 */
	public final boolean enable;
	
	
	/**
	 * The URL of the queue to publish the messages to.
	 */
	public final URI queueURL;
	
	
	/**
	 * The names of the JWT claims to include in the published messages,
	 * {@code null} to ignore.
	 */
	public final Set<String> includeClaims;
	
	
	/**
	 * If {@code true} all JWT claims are to be included in the published
	 * messages, {@link #includeClaims must be ignored then}.
	 */
	public final boolean includeAllClaims;
	
	
	/**
	 * System properties to include in the published messages, empty set if
	 * none.
	 */
	public final Set<String> includeSystemProperties;
	
	
	/**
	 * Creates a new configuration from the specified properties.
	 *
	 * @param props The properties.
	 *
	 * @throws PropertyParseException On a missing on invalid property.
	 */
	public Configuration(final Properties props)
		throws PropertyParseException {
		
		PropertyRetriever pr = new PropertyRetriever(props, true);
		
		enable = pr.getOptBoolean(PREFIX + "enable", false);
		
		if (! enable) {
			queueURL = null;
			includeClaims = null;
			includeAllClaims = true;
			includeSystemProperties = Collections.emptySet();
			return;
		}
		
		queueURL = pr.getURI(PREFIX + "queueURL");
		
		String includeClaimsString = pr.getOptString(PREFIX + "includeClaims", "*");
		
		if ("*".equals(includeClaimsString.trim())) {
			includeAllClaims = true;
			includeClaims = null;
		} else {
			includeAllClaims = false;
			includeClaims = new HashSet<>(StringListParser.parse(includeClaimsString));
		}
		
		includeSystemProperties = new HashSet<>(
			StringListParser.parse(
				pr.getOptString(PREFIX + "includeSystemProperties", "")));
	}
	
	
	@Override
	public void log() {
		Logger log = LogManager.getLogger("MAIN");
		log.info("[EVSQS0000] AWS SQS token event publisher:");
		log.info("[EVSQS0001] AWS SQS token event publisher enabled: {}", enable);
		
		if (! enable) {
			return;
		}
		
		log.info("[EVSQS0002] AWS SQS token event queue URL: {}", queueURL);
		log.info("[EVSQS0003] AWS SQS token event publisher includes all token claims: {}", includeAllClaims);
		if (! includeAllClaims) {
			log.info("[EVSQS0004] AWS SQS token event publisher token claims to include: {}", includeClaims);
		}
		log.info("[EVSQS0005] AWS SQS token event publisher system properties to include: {}", includeSystemProperties);
	}
}
