package com.nimbusds.openid.connect.provider.spi.events.aws.sqs;


import java.util.Map;

import com.nimbusds.jwt.JWTClaimsSet;
import net.minidev.json.JSONObject;


/**
 * Token claims filter.
 */
class ClaimsFilter {
	
	
	/**
	 * The configuration.
	 */
	private final Configuration config;
	
	
	/**
	 * Creates a new claims filter.
	 *
	 * @param config The configuration.
	 */
	ClaimsFilter(final Configuration config) {
		assert config != null;
		this.config = config;
	}
	
	
	/**
	 * Filters the specified JWT claims set.
	 *
	 * @param jwtClaimsSet The JWT claims set.
	 *
	 * @return The filtered claims as JSON object.
	 */
	JSONObject filter(final JWTClaimsSet jwtClaimsSet) {
		
		JSONObject out = new JSONObject();
		
		if (config.includeAllClaims) {
			out.putAll(jwtClaimsSet.toJSONObject());
		} else {
			for (Map.Entry<String,Object> en: jwtClaimsSet.toJSONObject().entrySet()) {
				if (config.includeClaims.contains(en.getKey())) {
					out.put(en.getKey(), en.getValue());
				}
			}
		}
		
		for (String sysPropName: config.includeSystemProperties) {
			String value = System.getProperty(sysPropName);
			if (value != null) {
				out.put(sysPropName, System.getProperty(sysPropName));
			}
		}
		
		return out;
	}
}
