package com.nimbusds.openid.connect.provider.spi.events.aws.sqs;


import java.util.Properties;

import com.nimbusds.jwt.JWTClaimsSet;
import junit.framework.TestCase;
import net.minidev.json.JSONObject;


public class ClaimsFilterTest extends TestCase {
	
	
	public void testPassAll()
		throws Exception {
		
		Properties props = new Properties();
		props.setProperty("op.events.sqsPublisher.enable", "true");
		props.setProperty("op.events.sqsPublisher.queueURL", "https://sqs.eu-central-1.amazonaws.com/673938396/MyQueue");
		props.setProperty("op.events.sqsPublisher.includeClaims", "*");
		Configuration config = new Configuration(props);
		ClaimsFilter filter = new ClaimsFilter(config);
		
		JWTClaimsSet jwtClaimsSet = new JWTClaimsSet.Builder()
			.issuer("https://c2id.com")
			.subject("alice")
			.claim("scope", "read write")
			.build();
		
		JSONObject jsonObject = filter.filter(jwtClaimsSet);
		assertEquals(jwtClaimsSet.getIssuer(), jsonObject.get("iss"));
		assertEquals(jwtClaimsSet.getSubject(), jsonObject.get("sub"));
		assertEquals(jwtClaimsSet.getStringClaim("scope"), jsonObject.get("scope"));
		assertEquals(3, jsonObject.size());
	}
	
	
	public void testIncludeSub()
		throws Exception {
		
		Properties props = new Properties();
		props.setProperty("op.events.sqsPublisher.enable", "true");
		props.setProperty("op.events.sqsPublisher.queueURL", "https://sqs.eu-central-1.amazonaws.com/673938396/MyQueue");
		props.setProperty("op.events.sqsPublisher.includeClaims", "sub");
		Configuration config = new Configuration(props);
		ClaimsFilter filter = new ClaimsFilter(config);
		
		JWTClaimsSet jwtClaimsSet = new JWTClaimsSet.Builder()
			.issuer("https://c2id.com")
			.subject("alice")
			.claim("scope", "read write")
			.build();
		
		JSONObject jsonObject = filter.filter(jwtClaimsSet);
		assertEquals(jwtClaimsSet.getSubject(), jsonObject.get("sub"));
		assertEquals(1, jsonObject.size());
	}
	
	
	public void testIncludeSubWithSystemPropertyTID()
		throws Exception {
		
		Properties props = new Properties();
		props.setProperty("op.events.sqsPublisher.enable", "true");
		props.setProperty("op.events.sqsPublisher.queueURL", "https://sqs.eu-central-1.amazonaws.com/673938396/MyQueue");
		props.setProperty("op.events.sqsPublisher.includeClaims", "sub");
		props.setProperty("op.events.sqsPublisher.includeSystemProperties", "tid");
		Configuration config = new Configuration(props);
		ClaimsFilter filter = new ClaimsFilter(config);
		
		System.setProperty("tid", "t1");
		
		JWTClaimsSet jwtClaimsSet = new JWTClaimsSet.Builder()
			.issuer("https://c2id.com")
			.subject("alice")
			.claim("scope", "read write")
			.build();
		
		JSONObject jsonObject = filter.filter(jwtClaimsSet);
		assertEquals(jwtClaimsSet.getSubject(), jsonObject.get("sub"));
		assertEquals("t1", jsonObject.get("tid"));
		assertEquals(2, jsonObject.size());
		
		System.clearProperty("tid");
	}
}
