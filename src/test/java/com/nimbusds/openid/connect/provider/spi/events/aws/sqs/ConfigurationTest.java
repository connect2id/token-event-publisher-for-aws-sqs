package com.nimbusds.openid.connect.provider.spi.events.aws.sqs;


import java.net.URI;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Properties;

import junit.framework.TestCase;


public class ConfigurationTest extends TestCase {
	
	
	public void testPrefix() {
		
		assertEquals("op.events.sqsPublisher.", Configuration.PREFIX);
	}
	
	
	public void testDisabled()
		throws Exception {
		
		Properties props = new Properties();
		props.setProperty("op.events.sqsPublisher.enable", "false");
		Configuration config = new Configuration(props);
		assertFalse(config.enable);
		assertNull(config.queueURL);
		assertNull(config.includeClaims);
		assertTrue(config.includeAllClaims);
		assertTrue(config.includeSystemProperties.isEmpty());
		config.log();
	}
	
	
	public void testMinimalConfig()
		throws Exception {
		
		Properties props = new Properties();
		props.setProperty("op.events.sqsPublisher.enable", "true");
		props.setProperty("op.events.sqsPublisher.queueURL", "https://sqs.eu-central-1.amazonaws.com/673938396/MyQueue");
		Configuration config = new Configuration(props);
		assertTrue(config.enable);
		assertEquals(URI.create("https://sqs.eu-central-1.amazonaws.com/673938396/MyQueue"), config.queueURL);
		assertNull(config.includeClaims);
		assertTrue(config.includeAllClaims);
		assertTrue(config.includeSystemProperties.isEmpty());
		config.log();
	}
	
	
	public void testIncludeAllClaims()
		throws Exception {
		
		Properties props = new Properties();
		props.setProperty("op.events.sqsPublisher.enable", "true");
		props.setProperty("op.events.sqsPublisher.queueURL", "https://sqs.eu-central-1.amazonaws.com/673938396/MyQueue");
		props.setProperty("op.events.sqsPublisher.includeClaims", "*");
		Configuration config = new Configuration(props);
		assertTrue(config.enable);
		assertEquals(URI.create("https://sqs.eu-central-1.amazonaws.com/673938396/MyQueue"), config.queueURL);
		assertNull(config.includeClaims);
		assertTrue(config.includeAllClaims);
		assertTrue(config.includeSystemProperties.isEmpty());
		config.log();
	}
	
	
	public void testConfig_filterJWTClaims()
		throws Exception {
		
		Properties props = new Properties();
		props.setProperty("op.events.sqsPublisher.enable", "true");
		props.setProperty("op.events.sqsPublisher.queueURL", "https://sqs.eu-central-1.amazonaws.com/673938396/MyQueue");
		props.setProperty("op.events.sqsPublisher.includeClaims", "iss sub iat");
		Configuration config = new Configuration(props);
		assertTrue(config.enable);
		assertEquals(URI.create("https://sqs.eu-central-1.amazonaws.com/673938396/MyQueue"), config.queueURL);
		assertEquals(new HashSet<>(Arrays.asList("iss", "sub", "iat")), config.includeClaims);
		assertFalse(config.includeAllClaims);
		assertTrue(config.includeSystemProperties.isEmpty());
		config.log();
	}
	
	
	public void testConfig_includeSysProps()
		throws Exception {
		
		Properties props = new Properties();
		props.setProperty("op.events.sqsPublisher.enable", "true");
		props.setProperty("op.events.sqsPublisher.queueURL", "https://sqs.eu-central-1.amazonaws.com/673938396/MyQueue");
		props.setProperty("op.events.sqsPublisher.includeSystemProperties", "tid, java.version");
		Configuration config = new Configuration(props);
		assertTrue(config.enable);
		assertEquals(URI.create("https://sqs.eu-central-1.amazonaws.com/673938396/MyQueue"), config.queueURL);
		assertNull(config.includeClaims);
		assertTrue(config.includeAllClaims);
		assertEquals(new HashSet<>(Arrays.asList("tid", "java.version")), config.includeSystemProperties);
		config.log();
	}
}
