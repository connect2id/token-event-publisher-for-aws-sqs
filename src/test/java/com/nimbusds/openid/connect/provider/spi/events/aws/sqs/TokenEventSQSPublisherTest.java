package com.nimbusds.openid.connect.provider.spi.events.aws.sqs;


import java.io.InputStream;
import java.net.URI;
import java.util.Arrays;
import java.util.HashSet;
import javax.servlet.ServletContext;

import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.oauth2.sdk.id.Issuer;
import com.nimbusds.openid.connect.provider.spi.InitContext;
import com.nimbusds.openid.connect.provider.spi.ServiceContext;
import com.nimbusds.openid.connect.provider.spi.crypto.JWTSigner;
import com.nimbusds.openid.connect.provider.spi.events.AccessTokenIssueEvent;
import com.nimbusds.openid.connect.provider.spi.events.EventContext;
import com.nimbusds.openid.connect.provider.spi.events.IDTokenIssueEvent;
import junit.framework.TestCase;
import net.minidev.json.JSONObject;
import org.infinispan.manager.EmbeddedCacheManager;


public class TokenEventSQSPublisherTest extends TestCase {
	
	
	public void testConfigFilePath() {
		
		assertEquals("/WEB-INF/tokenEventSQSPublisher.properties", TokenEventSQSPublisher.CONFIG_FILE_PATH);
	}
	
	
	// check result in AWS SQS console
	public void _testPublish_directJSONObject_empty()
		throws Exception {
		
		TokenEventSQSPublisher publisher = new TokenEventSQSPublisher();
		publisher.init(new MockInitContext());
		publisher.sendMessage(new JSONObject());
		publisher.shutdown();
	}
	
	
	// check result in AWS SQS console
	public void _testPublish_idTokenIssueEvent()
		throws Exception {
		
		TokenEventSQSPublisher publisher = new TokenEventSQSPublisher();
		publisher.init(new MockInitContext());
		
		publisher.idTokenIssued(
			new IDTokenIssueEvent(
				this,
				new JWTClaimsSet.Builder().subject("alice").build()),
			new EventContext() {
				@Override
				public JWTSigner getJWTSigner() {
					return null;
				}
				
				
				@Override
				public Issuer getIssuer() {
					return null;
				}
			}
		);
		
		publisher.shutdown();
	}
	
	
	// check result in AWS SQS console
	public void _testPublish_accessTokenIssueEvent()
		throws Exception {
		
		TokenEventSQSPublisher publisher = new TokenEventSQSPublisher();
		publisher.init(new MockInitContext());
		
		publisher.accessTokenIssued(
			new AccessTokenIssueEvent(
				this,
				new JWTClaimsSet.Builder().subject("bob").build()),
			new EventContext() {
				@Override
				public JWTSigner getJWTSigner() {
					return null;
				}
				
				
				@Override
				public Issuer getIssuer() {
					return null;
				}
			}
		);
		
		publisher.shutdown();
	}
	
	
	// check result in AWS SQS console
	public void _testPublish_accessTokenIssueEvent_includeSysProperty()
		throws Exception {
		
		TokenEventSQSPublisher publisher = new TokenEventSQSPublisher();
		
		System.setProperty("tid", "t1");
		System.setProperty("op.events.sqsPublisher.includeSystemProperties", "tid");
		
		publisher.init(new MockInitContext());
		
		publisher.accessTokenIssued(
			new AccessTokenIssueEvent(
				this,
				new JWTClaimsSet.Builder().subject("claire").build()),
			new EventContext() {
				@Override
				public JWTSigner getJWTSigner() {
					return null;
				}
				
				
				@Override
				public Issuer getIssuer() {
					return null;
				}
			}
		);
		
		publisher.shutdown();
		
		System.clearProperty("tid");
	}
	
	
	public void testIgnoreMissingConfigFile()
		throws Exception {
		
		TokenEventSQSPublisher publisher = new TokenEventSQSPublisher();
		
		publisher.init(new InitContext() {
			@Override
			public ServletContext getServletContext() {
				return null;
			}
			
			
			@Override
			public InputStream getResourceAsStream(String path) {
				return null; // config file not found
			}
			
			
			@Override
			public EmbeddedCacheManager getInfinispanCacheManager() {
				return null;
			}
			
			
			@Override
			public Issuer getIssuer() {
				return null;
			}
			
			
			@Override
			public Issuer getOPIssuer() {
				return null;
			}
			
			
			@Override
			public URI getTokenEndpointURI() {
				return null;
			}
			
			
			@Override
			public ServiceContext getServiceContext() {
				return null;
			}
		});
		
		assertFalse(publisher.isEnabled());
	}
	
	
	public void testEnableViaSysProps()
		throws Exception {
		
		TokenEventSQSPublisher publisher = new TokenEventSQSPublisher();
		
		System.setProperty("op.events.sqsPublisher.enable", "true");
		System.setProperty("op.events.sqsPublisher.queueURL", "https://sqs.eu-central-1.amazonaws.com/673938396/MyQueue");
		System.setProperty("op.events.sqsPublisher.includeClaims", "iss sub iat");
		
		publisher.init(new InitContext() {
			@Override
			public ServletContext getServletContext() {
				return null;
			}
			
			
			@Override
			public InputStream getResourceAsStream(String path) {
				return null; // config file not found
			}
			
			
			@Override
			public EmbeddedCacheManager getInfinispanCacheManager() {
				return null;
			}
			
			
			@Override
			public Issuer getIssuer() {
				return null;
			}
			
			
			@Override
			public Issuer getOPIssuer() {
				return null;
			}
			
			
			@Override
			public URI getTokenEndpointURI() {
				return null;
			}
			
			
			@Override
			public ServiceContext getServiceContext() {
				return null;
			}
		});
		
		assertTrue(publisher.isEnabled());
		
		assertTrue(publisher.getConfiguration().enable);
		assertEquals(URI.create("https://sqs.eu-central-1.amazonaws.com/673938396/MyQueue"), publisher.getConfiguration().queueURL);
		assertEquals(new HashSet<>(Arrays.asList("iss", "sub", "iat")), publisher.getConfiguration().includeClaims);
		assertTrue(publisher.getConfiguration().includeSystemProperties.isEmpty());
		
		System.clearProperty("op.events.sqsPublisher.enable");
		System.clearProperty("op.events.sqsPublisher.queueURL");
		System.clearProperty("op.events.sqsPublisher.includeClaims");
	}
}
