package com.nimbusds.openid.connect.provider.spi.events.aws.sqs;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import javax.servlet.ServletContext;

import com.nimbusds.oauth2.sdk.id.Issuer;
import com.nimbusds.openid.connect.provider.spi.InitContext;
import com.nimbusds.openid.connect.provider.spi.ServiceContext;
import org.infinispan.manager.EmbeddedCacheManager;


class MockInitContext implements InitContext {
	
	
	@Override
	public ServletContext getServletContext() {
		return null;
	}
	
	
	@Override
	public InputStream getResourceAsStream(String path) {
		try {
			return new FileInputStream(new File("test.properties"));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	
	@Override
	public EmbeddedCacheManager getInfinispanCacheManager() {
		return null;
	}
	
	
	@Override
	public Issuer getIssuer() {
		return null;
	}
	
	
	@Override
	public Issuer getOPIssuer() {
		return null;
	}
	
	
	@Override
	public URI getTokenEndpointURI() {
		return null;
	}
	
	
	@Override
	public ServiceContext getServiceContext() {
		return null;
	}
}
